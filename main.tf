terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

variable "instance_root_disk" {
  default = "10"
}

provider "yandex" {
  token     = var.yandex_cloud_token
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
}

resource "yandex_vpc_network" "default" {
  name = "default_yandex_test_kz"
}

resource "yandex_vpc_subnet" "subnet1" {
  name           = "subnet1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.default.id
  v4_cidr_blocks = ["10.0.0.0/24"]
}

resource "yandex_vpc_subnet" "subnet2" {
  name           = "subnet2"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.default.id
  v4_cidr_blocks = ["10.0.1.0/24"]
}


resource "yandex_vpc_address" "stat_address" {
  name = "alb-static-address"
  external_ipv4_address {
    zone_id = "ru-central1-a"
  }
}

resource "yandex_vpc_security_group" "ssh_access" {
  name        = "ssh-access"
  description = "Allow SSH access"
  network_id  = yandex_vpc_network.default.id

  ingress {
    description      = "SSH access"
    protocol         = "TCP"
    from_port        = 22
    to_port          = 22
    v4_cidr_blocks   = ["0.0.0.0/0"]
  }
  ingress {
    description      = "backend access"
    protocol         = "TCP"
    from_port        = 80
    to_port          = 80
    v4_cidr_blocks   = ["0.0.0.0/0"]
  }
  ingress {
    description    = "Allow ICMP"
    protocol       = "ICMP"
    v4_cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description      = "Allow all inbound traffic"
    protocol         = "ANY"
    v4_cidr_blocks   = ["0.0.0.0/0"]
  }
  egress {
    description      = "Allow all outbound traffic"
    protocol         = "ANY"
    v4_cidr_blocks   = ["0.0.0.0/0"]
  }
}

#create VMs
resource "yandex_compute_instance" "ubuntu_with_nginx_page1" {
  name                = "ubuntu-with-nginx-page1"
  folder_id           = var.folder_id
  platform_id         = "standard-v1"
  zone        = "ru-central1-a"
  resources {
    cores  = 2
    memory = 2
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet1.id
    nat       = true
    security_group_ids = [yandex_vpc_security_group.ssh_access.id]
  }

  boot_disk {
    initialize_params {
      image_id = "fd89cudngj3s2osr228p"  # Ubuntu image ID
      size     = 10
    }
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update -y",
      "sudo apt-get install -y nginx",
      "sudo systemctl enable nginx",
      "echo '<html><body>Hello page1 it is VM1</body></html>' | sudo tee /var/www/html/page1.html",
      "echo '<html><body>404 Not Found</body></html>' | sudo tee /var/www/html/index.html",
      "sudo systemctl start nginx",
      "sudo systemctl restart nginx",
      "echo 'Intallation Finished'"
    ]
    connection {
      type        = "ssh"
      host        = self.network_interface[0].nat_ip_address
      user        = "ubuntu"
      private_key = file("~/.ssh/id_rsa")
    }
  }
}


resource "yandex_compute_instance" "ubuntu_with_nginx_page2" {
  name                = "ubuntu-with-nginx-page2"
  folder_id           = var.folder_id
  platform_id         = "standard-v1"
  zone        = "ru-central1-b"
  resources {
    cores  = 2
    memory = 2
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet2.id
    nat       = true
    security_group_ids = [yandex_vpc_security_group.ssh_access.id]
  }
  boot_disk {
    initialize_params {
      image_id = "fd89cudngj3s2osr228p"  # Ubuntu image ID
      size     = 10
    }
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update -y",
      "sudo apt-get install -y nginx",
      "sudo systemctl enable nginx",
      "echo '<html><body>Hello page2 it is VM2</body></html>' | sudo tee /var/www/html/page2.html",
      "echo '<html><body>404 Not Found</body></html>' | sudo tee /var/www/html/index.html",
      "sudo systemctl start nginx",
      "sudo systemctl restart nginx",
      "echo 'Intallation Finished'"
    ]
    connection {
      type        = "ssh"
      host        = self.network_interface[0].nat_ip_address
      user        = "ubuntu"
      private_key = file("~/.ssh/id_rsa")
    }
  }
}
resource "yandex_compute_snapshot" "ubuntu-page1-snapshot" {
  name           = "ubuntu-page1-snapshot"
  source_disk_id = yandex_compute_instance.ubuntu_with_nginx_page1.boot_disk[0].disk_id
}
resource "yandex_compute_snapshot" "ubuntu-page2-snapshot" {
  name           = "ubuntu-page2-snapshot"
  source_disk_id = yandex_compute_instance.ubuntu_with_nginx_page2.boot_disk[0].disk_id
}

# create image from VM
resource "yandex_compute_image" "ubuntu-page1-image" {
  name        = "ubuntu-page1-image"
  source_snapshot = yandex_compute_snapshot.ubuntu-page1-snapshot.id
}

resource "yandex_compute_image" "ubuntu-page2-image" {
  name        = "ubuntu-page2-image"
  source_snapshot = yandex_compute_snapshot.ubuntu-page2-snapshot.id
}

#create VMs Group base on VM
resource "yandex_compute_instance_group" "vm1" {
  name                = "backend-group-1"
  folder_id           = var.folder_id
  service_account_id  = var.service_account_id
  instance_template {
    platform_id = "standard-v1"
    resources {
      memory        = 2
      cores         = 2
    }

    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
        image_id =  yandex_compute_image.ubuntu-page1-image.id  #"fd89k85hm37bk6bfrtd2"
        size     = 11
      }
    }

    network_interface {
      network_id         = yandex_vpc_network.default.id
      subnet_ids         = [yandex_vpc_subnet.subnet1.id]
      security_group_ids = [yandex_vpc_security_group.ssh_access.id]
      nat                = true
    }

     metadata = {
      foo      = "bar"
      ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
    }
  }

  scale_policy {
    fixed_scale {
      size = 2
    }
  }

  allocation_policy {
    zones = ["ru-central1-a"]
  }

  deploy_policy {
    max_unavailable = 2
    max_creating    = 2
    max_expansion   = 2
    max_deleting    = 2
  }
  application_load_balancer {
    target_group_name        = "alb-tg1"
  }

}

resource "yandex_compute_instance_group" "vm2" {
  name                = "backend-group-2"
  folder_id           = var.folder_id
  service_account_id  = var.service_account_id
  instance_template {
    platform_id = "standard-v1"
    resources {
      memory        = 2
      cores         = 2
    }

    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
        image_id = yandex_compute_image.ubuntu-page2-image.id #"fd89k85hm37bk6bfrtd2"
        size     = 11
      }
    }

    network_interface {
      network_id         = yandex_vpc_network.default.id
      subnet_ids         = [yandex_vpc_subnet.subnet2.id]
      security_group_ids = [yandex_vpc_security_group.ssh_access.id]
      nat                = true
    }

     metadata = {
      foo      = "bar"
      ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
    }
  }

  scale_policy {
    fixed_scale {
      size = 2
    }
  }

  allocation_policy {
    zones = ["ru-central1-b"]
  }

  deploy_policy {
    max_unavailable = 2
    max_creating    = 2
    max_expansion   = 2
    max_deleting    = 2
  }
  application_load_balancer {
    target_group_name        = "alb-tg2"
  }

}

resource "yandex_alb_backend_group" "page1_backend_group" {
  name = "page1-backend-group"
  http_backend {
    name = "page1-backend"
    weight = 1
    port = 80
    target_group_ids = [yandex_compute_instance_group.vm1.application_load_balancer[0].target_group_id]
    load_balancing_config {
      panic_threshold = 50
    }
    healthcheck {
      timeout = "1s"
      interval = "1s"
      http_healthcheck {
        path = "/page1.html"
      }
    }
    http2 = "false"
  }
}

resource "yandex_alb_backend_group" "page2_backend_group" {
  name = "page2-backend-group"
  http_backend {
    name = "page2-backend"
    weight = 1
    port = 80
    target_group_ids = [yandex_compute_instance_group.vm2.application_load_balancer[0].target_group_id]
    load_balancing_config {
      panic_threshold = 50
    }
    healthcheck {
      timeout = "1s"
      interval = "1s"
      http_healthcheck {
        path = "/page2.html"
      }
    }
    http2 = "false"
  }
}

resource "yandex_alb_http_router" "router" {
  name = "my-http-router"
  labels = {
    tf-label = "tf-label-value"
  }
}

resource "yandex_alb_virtual_host" "vhost" {
  name      = "my-virtual-host"
  http_router_id = yandex_alb_http_router.router.id
  route {
    name = "page1-route"
    http_route {
      http_match {
        path {
          exact = "/page1.html"
        }
      }
      http_route_action {
        backend_group_id = yandex_alb_backend_group.page1_backend_group.id
        timeout = "3s"
      }
    }
  }
  route {
    name = "page2-route"
    http_route {
      http_match {
        path {
          exact = "/page2.html"
        }
      }
      http_route_action {
        backend_group_id = yandex_alb_backend_group.page2_backend_group.id
        timeout = "3s"
      }
    }
  }
}

resource "yandex_alb_load_balancer" "alb" {
  name        = "my-load-balancer"

  network_id  = yandex_vpc_network.default.id

  allocation_policy {
    location {
      zone_id   = "ru-central1-a"
      subnet_id = yandex_vpc_subnet.subnet1.id
    }
    location {
      zone_id   = "ru-central1-b"
      subnet_id = yandex_vpc_subnet.subnet2.id
    }
  }

  listener {
    name = "my-listener"
    endpoint {
      address {
        external_ipv4_address {
        }
      }
      ports = [ 80 ]
    }
    http {
      handler {
        http_router_id = yandex_alb_http_router.router.id
      }
    }
  }

  log_options {
    discard_rule {
      http_code_intervals = ["HTTP_ALL"]
      discard_percent = 75
    }
  }
}

resource "yandex_dns_zone" "zone1" {
  folder_id   = var.folder_id
  name        = "zone1"
  description = "Public zone"
  zone        = "yandex-test-k.com."
  public      = true
}

resource "yandex_dns_recordset" "alb-record" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "yandex-test-k.com."
  ttl     = 600
  type    = "A"
  data    = [yandex_alb_load_balancer.alb.listener[0].endpoint[0].address[0].external_ipv4_address[0].address]
}