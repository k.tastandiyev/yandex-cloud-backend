# Terraform Quickstart with Yandex Cloud

This repository provides a quickstart guide for setting up and using Terraform with Yandex Cloud. It includes creating a service account key, configuring the Yandex CLI, and setting up Terraform to manage Yandex Cloud resources.

## Prerequisites

Ensure you have the following tools installed:
- [Terraform](https://yandex.cloud/ru/docs/tutorials/infrastructure-management/terraform-quickstart#install-terraform)
- [Terraform Resources](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/)

## Yandex CLI Commands

### List Service Accounts
```bash
yc iam service-account list
```

### Folder List
```bash
yc resource-manager folder list
```

### List of VM images
```bash
yc compute image list --folder-id standard-images
```


## Step-by-Step Guide

### Create a Service Account Key
```bash
yc iam key create \
  --service-account-id aje7rsoq4djdcef0p96a \
  --folder-name default \
  --output key.json
```
 
#### Example output:
```bash
id: aje0pgvl90q6bqjde81f
service_account_id: aje7rsoq4djdcef0p96a
created_at: "2024-06-02T17:36:02.029672123Z"
key_algorithm: RSA_2048
```


### Configure the Yandex CLI Profile
```bash
yc config set service-account-key key.json
yc config set cloud-id b1gb11uumb3l7dbv0m84
yc config set folder-id b1go8mj945fa66fkma5k
```

### Add Authentication Data to Environment Variables
```bash
export YC_TOKEN=$(yc iam create-token)
export YC_CLOUD_ID=$(yc config get cloud-id)
export YC_FOLDER_ID=$(yc config get folder-id)
```

### Configure Terraform CLI
```bash
nano ~/.terraformrc
```
#### Add the following block:
```bash
provider_installation {
  network_mirror {
    url = "https://terraform-mirror.yandexcloud.net/"
    include = ["registry.terraform.io/*/*"]
  }
  direct {
    exclude = ["registry.terraform.io/*/*"]
  }
}
```

### Set Up Terraform Configuration
#### Create a Terraform configuration file (main.tf) with the following content:
```bash
terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  zone = "ru-central1-a"
}
```

### Initialize Terraform
```bash
terraform init
```
### Apply Terraform Configuration
```bash
terraform apply
```
### Destroy Terraform Resources
```bash
terraform destroy
``` 


https://yandex.cloud/en/docs/tutorials/infrastructure-management/packer-quickstart